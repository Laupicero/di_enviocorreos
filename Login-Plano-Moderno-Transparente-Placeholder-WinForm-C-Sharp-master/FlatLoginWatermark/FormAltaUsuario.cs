﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FlatLoginWatermark
{
    public partial class FormAltaUsuario : Form
    {
        private SqlConnection conexion;
        string modoVentana;
        private string usuario;
        private string email;
        private string nombre;

        // Constructor para Rergistro
        public FormAltaUsuario(string modo, SqlConnection conexion)
        {
            InitializeComponent();
            this.conexion = conexion;
            this.modoVentana = modo;
            opcionEstiloFormulario();
        }

        // Constructor para Cambio Clave
        public FormAltaUsuario(string modo, string usuario, string email, string nombre, SqlConnection conexion)
        {
            InitializeComponent();

            this.conexion = conexion;
            this.usuario = usuario;
            this.email = email;
            this.nombre = nombre;            
            this.modoVentana = modo;
            opcionEstiloFormulario();
        }

        // ---------------------------------
        //      EVENTOS BOTONES
        // ---------------------------------

        // BOTÓN GRABAR
        // dependiendo de si ingresará a un nuevo usuario o edite la clave, hará un 'insert' o un 'update' a la DB
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            //Para registro de usuario
            if (this.modoVentana == "registro")
            {
                if (todosCamposRellenos())
                {
                    if(txtClave.Text == txtClaveConfirm.Text)
                    {
                        insertarUsuario();
                        borrarCampos();
                        
                    }
                    else
                        MessageBox.Show("Deben coincidir tanto la clave como su confirmación", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                    MessageBox.Show("Debe rellenar primero todos los campos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            //Para cambiar las contraseñas
            else
            {
                if(txtClave.TextLength > 0 && txtClaveConfirm.TextLength > 0)
                {
                    if (txtClave.Text == txtClaveConfirm.Text)
                        actualizarClave();
                    else
                        MessageBox.Show("Deben coincidir tanto la clave como su confirmación", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                    MessageBox.Show("Debe rellenar primero ambos campos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        


        // BOTÓN CANCELAR
        // dependiendo de si ingresará a un nuevo usuario o edite la clave, 
        // nos borrará/limpiará todos los campos no el de la clave
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            borrarCampos();
        }


        //BOTÓN CERRAR
        private void btncerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // ---------------------------------
        //      MÉTODOS AUXILIARES
        // ---------------------------------

        // Dependiendo de sí es para registrar usuario o cambiar la clave
        // tendrá habilitado/ cambiado diferentes elementos
        private void opcionEstiloFormulario()
        {
            if(this.modoVentana == "registro")
            {
                labelMensaje.Text = "Crear Nueva Cuenta de Usuario";
                txtUsuario.Enabled = true;
                txtClave.Enabled = true;
                txtClaveConfirm.Enabled = true;
                txtEmail.Enabled = true;
                txtNombre.Enabled = true;
            }
            else
            {
                labelMensaje.Text = "Cambio de Contraseña";
                txtUsuario.Enabled = false;
                txtUsuario.Text = this.usuario;

                txtClave.Enabled = true;
                txtClaveConfirm.Enabled = true;

                txtEmail.Enabled = false;
                txtEmail.Text = this.email;

                txtNombre.Enabled = false;
                txtNombre.Text = this.nombre;
            }
        }


        private void borrarCampos()
        {
            if (this.modoVentana == "registro")
            {
                txtUsuario.Clear();
                txtEmail.Clear();
                txtNombre.Clear();
                txtClave.Clear();
                txtClaveConfirm.Clear();
            }
            else
            {
                txtClave.Clear();
                txtClaveConfirm.Clear();
            }
        }


        // Nos comprobará si están rellenos todos los campos del formulario, a la hora de ingresar un nuevo usuario en la DB
        private bool todosCamposRellenos()
        {
            if (txtNombre.TextLength > 0 && txtUsuario.TextLength > 0 && txtEmail.TextLength > 0 && txtClave.TextLength > 0 && txtClaveConfirm.TextLength > 0)
                return true;
            else
                return false;
        }


        // ---------------------------------
        //      MÉTODOS AUXILIARES CRUD
        // ---------------------------------

        // Nos insertará un nuevo usuario en la DB
        private void insertarUsuario()
        {
            try
            {
                String query = "INSERT INTO Usuario(NOMBREUSUARIO, CONTRA, NOMBREC, EMAIL)" +
                    "VALUES('" +txtUsuario.Text+ "', '" +txtClave.Text+ "', '" +txtNombre.Text+ "', '" +txtEmail.Text+ "')";

                SqlCommand cmd = new SqlCommand(query, this.conexion);

                cmd.ExecuteNonQuery();
                MessageBox.Show("¡Nuevo Usuario Insertado con ÉXITO!", "OPERACIÓN REALIZADA");
            }
            catch (Exception e)
            {
                MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
            }
        }


        // Nos actualiza la contraseña de nuestro usuario
        private void actualizarClave()
        {
            try
            {
                String query = "UPDATE Usuario SET CONTRA = '" +txtClave.Text+ "' WHERE EMAIL = '" +txtEmail.Text+ "'";

                SqlCommand cmd = new SqlCommand(query, this.conexion);
                cmd.ExecuteNonQuery();

                MessageBox.Show("¡Usuario Actualizado con ÉXITO!", "OPERACIÓN REALIZADA");
                borrarCampos();

            }
            catch (Exception e)
            {
                MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FlatLoginWatermark
{
    public partial class FormBienvenida : Form
    {
        private string nombreUser;
        private SqlConnection conexion;

        // Constructor
        public FormBienvenida(string nombreUser, SqlConnection conexion)
        {
            InitializeComponent();

            this.nombreUser = nombreUser;
            this.conexion = conexion;

            rellenarDatosUsuario();
        }

        // Nos rellena los datos del usuario en los correspondientes Campos
        private void rellenarDatosUsuario()
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            String email = "";

            cmd = new SqlCommand("SELECT EMAIL FROM Usuario WHERE NOMBREUSUARIO='" +this.nombreUser+ "'", this.conexion);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
                email = dataReader["EMAIL"].ToString();

            dataReader.Close();

            labelNombreUser.Text = this.nombreUser;
            labelCorreo.Text = email;
        }

        // Botón cerrar Formulario
        private void btncerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

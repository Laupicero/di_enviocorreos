﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace FlatLoginWatermark
{
    public partial class FormRecuperacionContrasenna : Form
    {
        private SqlConnection conexion;

        //Constructor
        public FormRecuperacionContrasenna(SqlConnection conexion)
        {
            InitializeComponent();
            this.conexion = conexion;
        }


        // ---------------------------------
        //      EVENTOS BOTONES
        // ---------------------------------

        //Botón enviar
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if(txtEmail.TextLength > 0)
                comprobacionExisteUsuario();
            else
                MessageBox.Show("Inserte primero su Email", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        //Botón cerrar
        private void btncerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        // ---------------------------------
        //      MÉTODOS AUXILIARES
        // ---------------------------------

        // Nos comprueba a través del Email si existe el usuario
        // Y si así fuere, envíar los datos de recuperación a su correo
        private void comprobacionExisteUsuario()
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            string usuario = "";
            string email = "";
            string nombre = "";
            string contra = "";

            cmd = new SqlCommand("SELECT * FROM Usuario where EMAIL = '" + txtEmail.Text + "'", this.conexion);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                usuario = dataReader["NOMBREUSUARIO"].ToString();
                email = dataReader["EMAIL"].ToString();
                nombre = dataReader["NOMBREC"].ToString();
                contra = dataReader["CONTRA"].ToString();
            }
            dataReader.Close();

            // Si se han encontrado coincidencias
            if (datosEncontrados(usuario, email, nombre))
            {
                string remitente = "alumnos2damAlmunia@gmail.com";
                string miclave = "2dam2dam";
                string asunto = "Recuperación de contraseña";
                string mensaje = "Aquí, tiene su contraseña:" +contra+ "\nTenga cuidado, no vuelva a perderla";

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential(remitente, miclave),
                    EnableSsl = true,
                };
                smtpClient.Send(remitente, txtEmail.Text, asunto, mensaje);
                MessageBox.Show("Correo enviado a: " + txtEmail.Text);

            }
            else
                MessageBox.Show("¡DATOS NO ENCONTRADOS!" +
                    "\nCompruebe que su Email es correcto", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }



        // Comprobamos que los datos han sido encontrados
        private bool datosEncontrados(string usuario, string email, string nombre)
        {
            if (usuario.Length > 0 && email.Length > 0 && nombre.Length > 0)
                return true;
            else
                return false;
        }

        
    }
}

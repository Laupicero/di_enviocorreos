﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;

namespace WindowsFormsApp1
{
    public partial class SendEmailForm : Form
    {
        // variables globales
        private string remitente = "alumnos2damAlmunia@gmail.com";
        private string miclave = "2dam2dam ";
        private string destinatario = "llucbue379@iesalmunia.com";
        private string asunto = "Test Envío correo desde la App";
        private string mensaje1 = "aaaaaaaaaaaaa";
        private string mensaje2 = "<h2>Im testing this</h2><p>Please, help</p>";


        // Constructor
        public SendEmailForm()
        {
            InitializeComponent();
        }


        // Se envía un correo normal
        private void btn_EnvioCorreoNormal_Click(object sender, EventArgs e)
        {
            try
            {
                // OPCIÓN 1
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential(remitente, miclave),
                    EnableSsl = true,
                };
                smtpClient.Send(remitente, destinatario, asunto, mensaje1);


                //Opcion 2
                /*
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.Credentials = new NetworkCredential(remitente, miclave);
                smtpClient.EnableSsl = true;
                smtpClient.Send(remitente, destinatario, asunto, mensaje1);
                 */

                // OPCIÓN 3
                /*SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.Port = 587;
                smtpClient.Credentials = new NetworkCredential(remitente, miclave);
                smtpClient.EnableSsl = true;
                smtpClient.Send(remitente, destinatario, asunto, mensaje1);
                 */


                MessageBox.Show("Mensaje enviado con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:\n" + ex.Message);
            }            
        }


        // Se envía un correo con formato 'html'
        private void btn_EnvioCorreoHTML_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje2);
                correo.IsBodyHtml = true;

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);

                MessageBox.Show("Mensaje enviado con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:\n" + ex.Message);
            }
        }


        // Se envía un correo que puede contener ficheros de imágenes
        private void btn_EnvioCorreoFicheros1_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje2);
                correo.IsBodyHtml = true;
                correo.Attachments.Add(new Attachment("c:\\foto.jpg", MediaTypeNames.Image.Jpeg));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);

                MessageBox.Show("Mensaje enviado con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:\n" + ex.Message);
            }
        }


        // Se envía un correo que puede contener ficheros XML
        private void btn_EnvioCorreoFicheros2_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje2);
                correo.IsBodyHtml = true;
                correo.Attachments.Add(new Attachment("c:\\X401006296940901231220.xml", MediaTypeNames.Text.Xml));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);

                MessageBox.Show("Mensaje enviado con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:\n" + ex.Message);
            }
        }
    }
}

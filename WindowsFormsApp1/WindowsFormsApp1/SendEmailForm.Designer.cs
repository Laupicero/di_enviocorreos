﻿
namespace WindowsFormsApp1
{
    partial class SendEmailForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_EnvioCorreoNormal = new System.Windows.Forms.Button();
            this.btn_EnvioCorreoHTML = new System.Windows.Forms.Button();
            this.btn_EnvioCorreoFicheros1 = new System.Windows.Forms.Button();
            this.btn_EnvioCorreoFicheros2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_EnvioCorreoNormal
            // 
            this.btn_EnvioCorreoNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EnvioCorreoNormal.Location = new System.Drawing.Point(25, 22);
            this.btn_EnvioCorreoNormal.Name = "btn_EnvioCorreoNormal";
            this.btn_EnvioCorreoNormal.Size = new System.Drawing.Size(333, 45);
            this.btn_EnvioCorreoNormal.TabIndex = 0;
            this.btn_EnvioCorreoNormal.Text = "ENVÍO NORMAL";
            this.btn_EnvioCorreoNormal.UseVisualStyleBackColor = true;
            this.btn_EnvioCorreoNormal.Click += new System.EventHandler(this.btn_EnvioCorreoNormal_Click);
            // 
            // btn_EnvioCorreoHTML
            // 
            this.btn_EnvioCorreoHTML.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EnvioCorreoHTML.Location = new System.Drawing.Point(25, 91);
            this.btn_EnvioCorreoHTML.Name = "btn_EnvioCorreoHTML";
            this.btn_EnvioCorreoHTML.Size = new System.Drawing.Size(333, 41);
            this.btn_EnvioCorreoHTML.TabIndex = 1;
            this.btn_EnvioCorreoHTML.Text = "ENVÍO HTML";
            this.btn_EnvioCorreoHTML.UseVisualStyleBackColor = true;
            this.btn_EnvioCorreoHTML.Click += new System.EventHandler(this.btn_EnvioCorreoHTML_Click);
            // 
            // btn_EnvioCorreoFicheros1
            // 
            this.btn_EnvioCorreoFicheros1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EnvioCorreoFicheros1.Location = new System.Drawing.Point(25, 155);
            this.btn_EnvioCorreoFicheros1.Name = "btn_EnvioCorreoFicheros1";
            this.btn_EnvioCorreoFicheros1.Size = new System.Drawing.Size(333, 44);
            this.btn_EnvioCorreoFicheros1.TabIndex = 2;
            this.btn_EnvioCorreoFicheros1.Text = "ENVÍO CON IMÁGENES";
            this.btn_EnvioCorreoFicheros1.UseVisualStyleBackColor = true;
            this.btn_EnvioCorreoFicheros1.Click += new System.EventHandler(this.btn_EnvioCorreoFicheros1_Click);
            // 
            // btn_EnvioCorreoFicheros2
            // 
            this.btn_EnvioCorreoFicheros2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EnvioCorreoFicheros2.Location = new System.Drawing.Point(25, 223);
            this.btn_EnvioCorreoFicheros2.Name = "btn_EnvioCorreoFicheros2";
            this.btn_EnvioCorreoFicheros2.Size = new System.Drawing.Size(333, 44);
            this.btn_EnvioCorreoFicheros2.TabIndex = 3;
            this.btn_EnvioCorreoFicheros2.Text = "ENVÍO FICHERO XML";
            this.btn_EnvioCorreoFicheros2.UseVisualStyleBackColor = true;
            this.btn_EnvioCorreoFicheros2.Click += new System.EventHandler(this.btn_EnvioCorreoFicheros2_Click);
            // 
            // SendEmailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 284);
            this.Controls.Add(this.btn_EnvioCorreoFicheros2);
            this.Controls.Add(this.btn_EnvioCorreoFicheros1);
            this.Controls.Add(this.btn_EnvioCorreoHTML);
            this.Controls.Add(this.btn_EnvioCorreoNormal);
            this.Name = "SendEmailForm";
            this.Text = "E N V I O   C O R R E O";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_EnvioCorreoNormal;
        private System.Windows.Forms.Button btn_EnvioCorreoHTML;
        private System.Windows.Forms.Button btn_EnvioCorreoFicheros1;
        private System.Windows.Forms.Button btn_EnvioCorreoFicheros2;
    }
}

